﻿using UnityEngine;
using System.Collections;

public class Candys : MonoBehaviour {

    public ControlP1 player;

    // Use this for initialization
    void Start() {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ControlP1>();
        //player = player.GetComponent<ControlP1>();
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            player.Candys(1);
            if (player.curHeart == 10) {
                player.curHeart += 1;
            }
            
        }
    }
}
