﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUB : MonoBehaviour {

    public Sprite[] HeartSprites;

    public Image HeartUI;

    public GameObject player;

    private ControlP1 playerControl;

	// Use this for initialization
	void Start () {
        playerControl = player.GetComponent<ControlP1>();
    }

    // Update is called once per frame
    void Update () {
        HeartUI.sprite = HeartSprites[playerControl.curHeart];
	}
}
