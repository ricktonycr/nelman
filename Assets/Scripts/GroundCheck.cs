﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

    private ControlP1 player;

	// Use this for initialization
	void Start () {
        player = gameObject.GetComponentInParent<ControlP1>();
	}

    void OnTriggerEnter2D(Collider2D col) {
        player.grounded = true;
    }

    void OnTriggerStay2D(Collider2D col) {
        player.grounded = true;
    }

    void OnTriggerExit2D(Collider2D col) {
        player.grounded = false;
    }

}
