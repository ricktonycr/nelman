﻿using UnityEngine;
using System.Collections;

public class EnemyInvese : MonoBehaviour
{

    public float velocity = 1f;

    public Transform sightStart;
    public Transform sightEnd;

    private Rigidbody2D rigi;

    public bool colliding;

    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rigi.velocity = new Vector2(-velocity, rigi.velocity.y);
        colliding = Physics2D.Linecast(sightStart.position, sightEnd.position);

        if (colliding)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            velocity *= -1;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }
}