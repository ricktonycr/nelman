﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CANDY : MonoBehaviour
{

    public Sprite[] CandySprites;

    public Image CandyUI;

    public GameObject player;

    private ControlP1 playerControl;

    // Use this for initialization
    void Start()
    {
        playerControl = player.GetComponent<ControlP1>();
    }

    // Update is called once per frame
    void Update()
    {
		CandyUI.sprite = CandySprites[playerControl.curCandy];
    }
}
