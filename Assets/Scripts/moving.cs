﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moving : MonoBehaviour {
	private Animator ani;

	// Use this for initialization
	void Start () {

		ani = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis ("Horizontal") < -0.1f) {
			transform.localScale = new Vector3 (-1, 1, 1);
		} else if (Input.GetAxis ("Horizontal") > 0.1f) {
			transform.localScale = new Vector3 (1, 1, 1);
		}

		if (Input.GetAxis ("Horizontal") < -0.01f) {
			ani.SetBool ("izq", true);
			ani.SetBool ("der", false);
		} else if (Input.GetAxis ("Horizontal") > 0.01f) {
			ani.SetBool ("der", true);
			ani.SetBool ("izq", false);
		} else {
			ani.SetBool ("izq", false);
			ani.SetBool ("der", false);
		}
	}
}
