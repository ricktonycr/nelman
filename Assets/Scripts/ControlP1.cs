﻿using UnityEngine;
using System.Collections;

public class ControlP1 : MonoBehaviour {

    public float maxSpeed = 3f;
    public int curHeart = 3;
    public int maxHeart = 8;

    public int curCandy = 0;
    public int maxCandy = 10;

    public float velocidad = 50f;
    public float fuerzaSalto = 500f;
    public bool grounded;

    private Rigidbody2D rb2d;
    private Animator ani;

	// Use this for initialization
	void Start () {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        ani = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        ani.SetBool("salta", grounded);
        ani.SetFloat("velocidadX", Mathf.Abs(Input.GetAxis("Horizontal")));

		if (Input.GetAxis ("Horizontal") < -0.1f) {
			transform.localScale = new Vector3 (-1, 1, 1);
		}
		if (Input.GetAxis ("Horizontal") > 0.1f) {
			transform.localScale = new Vector3 (1, 1, 1);
		}
        if (Input.GetButtonDown("Jump") && grounded) {
            rb2d.AddForce(Vector2.up * fuerzaSalto);
        }
    }

    void FixedUpdate() {
        float h = Input.GetAxis("Horizontal");
        rb2d.AddForce((Vector2.right * velocidad) * h);

        if (rb2d.velocity.x > maxSpeed) {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }

        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }
    }

    public void Damage(int dmg) {
        curHeart -= dmg;
    }

    public void Candys(int dmg) {
        curCandy = curCandy + dmg;
    }

    public IEnumerator Knockback(float knockDur, float knockbackPwr, Vector3 knockbackDir) {

        float timer = 0;
        while (knockDur > timer) {
            timer += Time.deltaTime;
            rb2d.AddForce(new Vector3(knockbackDir.x * - 50, knockbackDir.y * -knockbackPwr, transform.position.z));
        }

        yield return 0;
    }

    void OnTriggerEnter2D (Collider2D col) {
        if (col.CompareTag("Coin")) {
            Candys(1);
            if (curCandy == 10) {
                curHeart += 1;
            }
            Destroy(col.gameObject);
        }
    }
}
