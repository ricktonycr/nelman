﻿using UnityEngine;
using System.Collections;

public class Espinas : MonoBehaviour {

    public ControlP1 player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ControlP1>();
        //player = player.GetComponent<ControlP1>();
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            player.Damage(1);
            if (player.curHeart == 0) {
                Application.LoadLevel(Application.loadedLevel);
                player.curHeart = 3;
            }

            StartCoroutine(player.Knockback(0.02f, 150, player.transform.position));
        }
    }
}
